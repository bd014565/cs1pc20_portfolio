#Analysis of Bash exercise

1. .**$ mkdir -p $HOME/portfolio/week1 ; cd $HOME/portfolio/week1** *makes directory then changes directories meaning it will move the command prompt into that folder*
*-p will create sub directories of a directory, if parent directory already exists it will not produce an error instead will create a sub directory*
2. **$ cd ~**
*moves command prompt back to root folder*
3. **$ rm -r portfolio**
*removes all content of portfolio including the directory*
4. **$ mkdir -p $HOME/portfolio/week1 & cd $HOME/portfolio/week1**
*failed as & evaluates both sides of the expression instead of left to right*
5. **$ cd ~**
6. **$ rm -r portfolio**
7. **$ mkdir -p $HOME/portfolio/week1 && cd $HOME/portfolio/week1**
*successful as && evaluates the left hand side and only executes second command based on first command*
8. **$ echo "Hello World"**
*outputs a string*
9. **$ echo Hello, World**
*outputs a text*
10. **$ echo Hello, world; Foo bar**
*sets contents of placeholder variable* 
11. **$ echo Hello, world!**
12. **$ echo "line one";echo "line two"**
*outputs strings to seperate lines*
13. **$ echo "Hello, world > readme"**
*outputs string to command line*
14. **$ echo "Hello, world" > readme**
*sets contents of file readme
15. **$ cat readme**
*displays content of file*
16. **$ example="Hello, World"**
17. **$ echo $example**
*displays contents of file*
18. **$ echo ’$example’**
*prints filename*
19. **$ echo "$example"**
*prints contents of file*
20. **$ echo "Please enter your name."; read example**
*reads input to file called example*
21. **$ echo "Hello $example"**
*uses contents of file*
22. **$ three=1+1+1;echo $three**
*outputs contents of variable*
23. **$ bc**
*Arithmetic calculator*
24. **$ echo 1+1+1 | bc**
*calculator executes arithmetic operators*
25. **$ let three=1+1+1;echo $three**
*let declares a permanent variable*
26. **$ echo date**
*displays date*
27. **$ cal**
*displays calendar*
28. **$ which cal**
*shows location of calendar file*
29. **$ /bin/cal**
30. **$ $(which cal)**
*substitute contents of brackets to be run without knowing exact location of calendar file*
31. **$ ‘which cal‘**
32. **$ echo "The date is $(date)"**
33. **$seq09**
*displays number sequence each line seperate*
34. **$seq09|wc-l**
*displays number of lines counted*
35. **$ seq 0 9 > sequence**
*puts contents into file*
36. **$ wc -l < sequence**
*displays number of lines in sequence file*
37. **$ for I in $(seq 1 9) ; do echo $I ; done**
*loop that outputs 9 values of l*
38. **$ (echo -n 0 ; for I in $(seq 1 9) ; do echo -n +$I ; done ; echo) | bc**
*-n prints line without new line created,displays arithmetic to be calculated by bc command 0+1+2+3+4+5+6+7+8+9*
39. **$ echo -e ‘#include <stdio.h>\nint main(void) \n{\n printf(“Hello World\\n”);\n return 0;\n}’ > hello.c**
40. **$ cat hello.c**
41. **$ gcc hello.c -o hello**
42. **$ ./hello**

