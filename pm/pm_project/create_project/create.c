// Program to create a directory given name by user
#include <stdio.h>
#include <sys/stat.h>

// main will take parameter from the user
int main(int argc, char* argv[])
{
// use the name to create a directory
  int retval;
 retval=mkdir(argv[1], 0777);

// check to see if the directory already existed.
  if(retval)
  {
    printf("ERROR!  %d\n", retval);
  }
// finish up.
  return retval;
}
