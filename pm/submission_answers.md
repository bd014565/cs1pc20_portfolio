**1.Abort if requested project/feature name already exists under 'root' 
folder. Here 'root' does not mean the / root of the file system, but the 
folder from which the program is run.**

* int main(int argc, char* argv[])
* {
* int retval;
* retval=mkdir(argv[1], 0777);
*
* if(retval)
*  {
*    printf("ERROR!  %d\n", retval);
*  }
* return retval;
* }
*
**2. Be able to create basic file structure for project**

* cd ~/portfolio/pm    //after mkdir
* mkdir create_project
* mkdir add_feature
* mkdir add_tag
* mkdir find_tag
* mkdir move_by_tag
* mkdir output_svg

**3. Initialise git repository**

* git init portfolio
* cd portfolio
* git status
* git remote add origin https://csgitlab.reading.ac.uk/bd014565/cs1pc20_portfolio.git
* git push --set-upstream origin main

**4. Feature management**

create 2 files with functions to be called

* nano F1                |        nano F2.1
                             
* #/bin/bash                |     #/bin/bash

* Function feature1() {   |        Function feature2.1() { 

* echo "Error: File exists" |    echo "Error: File exists"
---------------------------------------------------------

* git branch feature
* git switch feature


