#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/** define some constant values for the size of the data
* noting of course that if your data needs bigger values, you have to
* edit the source and change the constants define here
*/
 #define COM_SIZ 60
 #define ARG_SIZ 1024
 #define RES_SIZ 1024

/**  \fn int main (int argc, char *argv[])
  this test program calls an existing executable and checks teh outputs to 
  standard output meet the expected value .
  it should be called with:
  test_outputs <filename which contains test definitions>
*/

int main(int argc, char *argv[]) {
  FILE *fp;  /**<  fp is a pointer used to give access to the file descriptor of the pipe */
  FILE *tests;
  char command[COM_SIZ];
  char arguments[ARG_SIZ];
  char expected[RES_SIZ];
  char actual[RES_SIZ];
  char command_line[COM_SIZ + ARG_SIZ];

  /** try to open the file named on the command line */
  tests = fopen(argv[1], "rt");
  if (tests == NULL) {
    printf("error opening the file %s\n", argv[1]);
    return 1;
}

/** we will read each line from the file.
* These should be structured as:
* command to run
* inputs 
* expected output 

* Note: this could go horribly wrong if the input file is no properly 
* formatted
*/
while (fgets(command, COM_SIZ, tests) != NULL) {
 fgets(arguments, ARG_SIZ, tests);
 fgets(expected, RES_SIZ, tests);
 
/** string handling in C can be cumbersome.
* typically suggestions online make use of "malloc" and
* "strcpy" and "strcat"
* but these complicate things and are arguably not good practice 
* strtok gives us a useful shortcut to 
* remove newlines (the way it used here)
*/
  strtok(command, "\n");
  snprintf(command_line, sizeof command_line, "%s %s", command, arguments);
/** now we call the command, with the arguments and capture the results
*so we can compare it to the expected result.
* the "popen" command opens a special type of file called a pipe
*/

fp = popen(command_line,"r");
if (fp == NULL) {
  printf("Failed to run command\n");
  exit(1);
}
/** (fill in this comment!)
*/
char message[RES_SIZ + RES_SIZ + 21];

while(fgets(actual, sizeof actual, fp) != NULL) {
   /** we create a message to let us know what was expected and what we got
   * note that we have split the next line across multiple lines! That is OK
   * we are allowed to that.
   */
  snprintf(message, sizeof message, "%s %s %s %s",
	"expected ", expected, " and got ", actual);
  printf("%s", message);
  /** Because wewant the test suite to keep running, we can us an if statement
  * rather than the assert function
  */
   if(!strcmp(actual, expected)) {
     printf("OK\n");
   }
   else { 
     printf("FAILED\n");
   }
  /** if we dont close file handle, we risk using up machine resource
  */
  pclose(fp);
  }
 }
  fclose(tests);

  return 0;
}
 
