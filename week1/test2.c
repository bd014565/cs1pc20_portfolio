#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
/* \fn int main(int argc, char *argv[])
    
    This test program calls an exisintg executable and checks the output 
    to start output meet the expected values.
    \todo change to allow different programs to be tested
    \todo change to be able to read a file with the expected outputs
*/
int main(int argc, char *argv[]) {
  FILE *fp;      /**< A pointer used to give access to the file descriptor of the pipe */
  char path[1035];  /**< An array of type char used as a buffer for reading from the pipe */

  fp = popen("/Users/eugenecullen/firstproj/helloworld/test", "r");
  if (fp == NULL){
    printf("Failed to run command\n");
    exit(1);
  }

  while(fgets(path, sizeof(path), fp) != NULL){
  	 printf("got %s", path);
   	 assert(!strcmp(path, "Hello world\n Hello gang\n"));
   	 }
  pclose(fp);
  return 0;
  }
