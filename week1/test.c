#include <stdio.h>
#include <assert.h>
#include "hello.h"

/** \fn int main(void)
    \brief This is for testing a library stored in libhello.a which writes 
    a string to standard output
    This tests by using an assert statement to check the results returned 
    by the library function
*/
int main(void) {
  assert(0==greet());
  return 0;
}
