#include <stdio.h>
/** \fn int greet(void)
    \brief Function prints Hello world

    Basic Function to demonstrate coding practices, can be modified later 
    to take an argument for the string to be printed
*/
int greet(void) {
  printf("Hello world\n");
  return 0;
}
